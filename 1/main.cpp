#include "queue.h"
#include <iostream>


using std::cout;
using std::cin;
using std::endl;



int main()
{
	queue* q = new queue;
	initQueue(q, 3); //initiate queue of size 3
	enqueue(q, 1);
	enqueue(q, 4);
	enqueue(q, 9);

	while (!isEmpty(q))
	{
		std::cout << dequeue(q) << std::endl;
	}

	cleanQueue(q);
	delete q;


	system("PAUSE");

	return 0;
	
}