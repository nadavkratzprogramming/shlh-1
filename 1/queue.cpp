#include "queue.h"


void initQueue(queue *q, int size)
{
	q->count = 0;
	q->maxSize = size;
	q->empty = true;
	q->elements = new unsigned int[q->maxSize];
}
void cleanQueue(queue *q)
{
	delete[] q->elements;
}


bool isEmpty(queue *q){
	return q->empty;
}
bool isFull(queue *q)
{
	return (q->count == q->maxSize);
}



void enqueue(queue *q, int newValue)
{
	if (!isFull(q))
	{
		q->elements[q->count] = newValue;
		q->count++;
		q->empty = false;

	}
}

int dequeue(queue *q)
{
	int ans = -1;
	if (!isEmpty(q))
	{
		ans = q->elements[0];

		int i;
		for (i = 1; i < q->count; ++i)
		{
			q->elements[i - 1] = q->elements[i];
		}

		q->count--;
		q->empty = (q->count == 0);
	}


	return ans;

}

