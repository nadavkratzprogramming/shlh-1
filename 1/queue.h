#ifndef QUEUE_H
#define QUEUE_H


/* a queue contains positive integer values. */
struct  queue
{
	int maxSize;
	int count;
	bool empty;
	unsigned int *elements;
};

typedef struct queue queue;

void initQueue(queue *q, int size);
void cleanQueue(queue *q);

void enqueue(queue *q, int newValue);
int dequeue(queue *q); // return element in top of queue, or 0 if empty

bool isEmpty(queue *s);
bool isFull(queue *s);

#endif /* QUEUE_H */