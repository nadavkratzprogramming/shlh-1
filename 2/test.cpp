#include "linkedList.h"
#include "stack.h"
#include "utils.h"
#include <iostream>

using namespace std;

int main()
{
	cout << "test linkedList" << endl;
	linkedList* list = new linkedList;
	initList(list);
	add(list, 1);
	add(list, 4);
	add(list, 9);
	cout << "the numbers are: 1 4 9" << endl;
	cout << "Your numbers in reverse order are:";
	for (int i = 0; i<3; i++)
	{
		cout << " "<< removeHead(list);
	}
	cout << endl;
	cleanList(list);//should do nothing only a sanity check
	delete list;

	cout << "test stack" << endl;
	stack *s = new stack;
	initStack(s);
	push(s, 2);
	push(s, 5);
	push(s, 7);
	cout << "the numbers are: 2 5 7" << endl;
	cout << "Your numbers in reverse order are:";
	for (int i = 0; i<3; i++)
	{
		cout << " " <<pop(s);
	}
	cout << endl;
	cleanStack(s);//should do nothing a sanity check
	delete s;


	cout << "test reverse" << endl;
	int numbers[5];
	cout << "the numbers are:" ;
	for (int i=0;i<5;i++)
	{
		numbers[i]=i;
		cout << " " << i;
	}
	cout << endl;
	reverse(numbers,5);

	cout << "Your numbers in reverse order are:";
	for(int i=0;i<5;i++)
	{
		cout << " " <<numbers[i];
	}
	cout <<endl;


	cout << "test reverse10" << endl;
	int* revArr = reverse10();
	cout << "Your numbers in reverse order are:";
	for (int i = 0; i<10; i++)
	{
		cout << " " << revArr[i];
	}
	cout << endl;
	delete revArr;
	
	system("pause");

}