#include "utils.h"
#include "stack.h"

#include<iostream>

void reverse(int *nums, int size)
{
	stack *s = new stack;
	initStack(s);
	int i;
	for (i = 0; i<size; i++)
	{
		push(s, nums[i]);
	}

	i = 0;
	while (!isEmpty(s))
	{
		nums[i++] = pop(s);
	}

	cleanStack(s);
	delete s;

}

int* reverse10()
{
	int* arr = new int[10];
	int input;
	for (int i = 0; i < 10; i++)
	{
		std::cout << "Enter a number: ";
		std::cin >> input;
		arr[i]=input;
		
	}
	reverse(arr,10);
	return arr;
}