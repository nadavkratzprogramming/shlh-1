#ifndef LINKEDLIST_H
#define LINKEDLIST_H

/* a link that contains a positive integer value*/
struct link
{
	unsigned value;
	struct link *next;
} ;

typedef struct link link;

typedef struct
{
	link *head;
} linkedList ;

void initList(linkedList *list);//initialize list
void add(linkedList *list , int newValue); // add new link in the beginning of list with newValue in it
int removeHead(linkedList *list); // return the value from the first link in list, and change the first link to head->next.return 0 if list is empty
void cleanList(linkedList *list); //delete all links from memory


#endif /*LINKEDKIST_H*/