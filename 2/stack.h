#ifndef STACK_H
#define STACK_H

#include "linkedList.h"

/* a positive-integer value stack, with no size limit */
typedef struct
{
	int _count;
	linkedList* _elements;
} stack;

void push(stack *s, int element);
int pop(stack *s);

void initStack(stack *s);
void cleanStack(stack *s);

bool isEmpty(stack* s);
bool isFull(stack* s);

#endif /*STACK_H*/